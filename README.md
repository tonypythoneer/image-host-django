# image-host-django

Upload images and serve images

```txt
├── app
│   ├── image_host - project core
│   └── membership - record BASIC, PREMIUM, ENTERPRISE
└── doc - tech doc
   ├── project.postman_collection.json - postman for api
   └── workflow.svg - diagram
```

## Requirements

The lib purpose with explaination

```txt
Django==4.0.5 - required
mysqlclient==2.1.1 - access mysql
djangorestframework==3.13.1 - required
Pillow==9.2.0 - process image
minio==7.1.10 - access local s3 storage
```

## Infra

Based on docker-compose

```
redis - record token
mysql - serving django for storing data
minio - store static files
web - django application
adminer - local mysql GUI
redisinsight - local redis GUI
```
