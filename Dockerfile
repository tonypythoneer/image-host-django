FROM python:3.10-slim

WORKDIR /app
COPY requirements.txt /app/

RUN apt-get -y update
RUN apt-get -y install python3-dev default-libmysqlclient-dev build-essential
RUN pip install -r requirements.txt

COPY ./app /app/


CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
