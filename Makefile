# make up
.PHONY: up
up:
	docker-compose up

# make down
.PHONY: down
down:
	docker-compose down

# make clean_docker
.PHONY: clean_docker
clean_docker:
	docker container prune
	docker image prune -a

# make migrate
.PHONY: migrate
migrate:
	docker-compose run web python manage.py migrate

# make createsuperuser
.PHONY: createsuperuser
createsuperuser:
	docker-compose run web python manage.py createsuperuser

# make sh
.PHONY: sh
sh:
	docker-compose exec -it web bash
