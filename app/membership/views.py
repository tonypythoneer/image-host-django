from io import BytesIO
from PIL import Image
from rest_framework.views import APIView
from rest_framework.response import Response
from django.conf import settings
from django.contrib.auth.models import User

from .models import Membership

MINIO_CLIENT = settings.MINIO_CLIENT

class MemberImage(APIView):

    def get(self, request, username=None, *args, **kwargs):
        user = User.objects.filter(username=username).first()
        if not user:
            return Response("not found user")

        if user.membership.tier == Membership.TEIR_ENTERPRISE:
            return Response({
                "origin": MINIO_CLIENT.presigned_get_object("project", f"{username}/origin"),
                "200_200": MINIO_CLIENT.presigned_get_object("project", f"{username}/image_200_200"),
                "400_400": MINIO_CLIENT.presigned_get_object("project", f"{username}/image_400_400"),
            })

        if user.membership.tier == Membership.TEIR_PREMIUM:
            return Response({
                "origin": MINIO_CLIENT.presigned_get_object("project", f"{username}/origin"),
                "200_200": MINIO_CLIENT.presigned_get_object("project", f"{username}/image_200_200"),
                "400_400": MINIO_CLIENT.presigned_get_object("project", f"{username}/image_400_400"),
            })
        
        return Response({
            "200_200": MINIO_CLIENT.presigned_get_object("project", f"{username}/image_200_200"),
        })

    def post(self, request, username=None, *args, **kwargs):
        does_exist = User.objects.filter(username=username).exists()
        if not does_exist:
            return Response("not found user")

        image = request.FILES.get('image', None)
        if image is None:
            return Response("no image")

        ct = image.content_type
        if ct != 'image/png' and ct != 'image/jpg':
            return Response("image must be png or jpg")
        print("print===")

        format = ct.split("/")[-1].upper()

        image_file = image.open()
        i = Image.open(image_file)
        
        image_format = ct.split("/")[-1].upper()

        i_temp = BytesIO()
        i.save(i_temp, format=image_format)
        i_size = i_temp.tell()
        i_temp.seek(0)
    
        i_200_200 = i.resize((200, 200))
        i_200_200_temp = BytesIO()
        i_200_200.save(i_200_200_temp, format=image_format)
        i_200_200_size = i_200_200_temp.tell()
        i_200_200_temp.seek(0)

        i_400_400 = i.resize((400, 400))
        i_400_400_temp = BytesIO()
        i_400_400.save(i_400_400_temp, format=format)
        i_400_400_size = i_400_400_temp.tell()
        i_400_400_temp.seek(0)

        r1 = MINIO_CLIENT.put_object("project", f"{username}/origin", i_temp, content_type=ct, length=i_size)
        r2 = MINIO_CLIENT.put_object("project", f"{username}/image_200_200", i_200_200_temp, content_type=ct, length=i_200_200_size)
        r3 = MINIO_CLIENT.put_object("project", f"{username}/image_400_400", i_400_400_temp, content_type=ct, length=i_400_400_size)

        return Response("complete")
