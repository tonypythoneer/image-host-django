from django.db import models
from django.dispatch import receiver
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ObjectDoesNotExist


class Membership(models.Model):
    TEIR_BASIC = 'BASIC'
    TEIR_PREMIUM = 'PREMIUM'
    TEIR_ENTERPRISE = 'ENTERPRISE'

    class Teirs(models.TextChoices):
        BASIC = 'BASIC', _('Basic')
        PREMIUM = 'PREMIUM', _('Premium')
        ENTERPRISE = 'ENTERPRISE', _('Enterprise')

    user = models.OneToOneField(
        User, 
        on_delete=models.CASCADE,
        primary_key=True,
    )
    tier = models.CharField(
        max_length=20, 
        choices=Teirs.choices, 
        default=Teirs.BASIC)


@receiver(models.signals.post_save, sender=User)
def execute_after_save(sender, instance: User, created: bool, *args, **kwargs):
    if created:
        Membership.objects.create(user=instance)
        print(f'create a new membership for user_id:{instance.id}')
