from .views import MemberImage

from django.urls import path, include

urlpatterns = [
    path('image/<username>', MemberImage.as_view()),
]
