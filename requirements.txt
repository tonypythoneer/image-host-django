Django==4.0.5
mysqlclient==2.1.1
djangorestframework==3.13.1
Pillow==9.2.0
minio==7.1.10
